/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dataox.elevator;

/**
 *
 * @author arkhi
 */
public class Passenger {
    
    private int destinationFloor;
    private int currentFloor;
	
    public int getDestinationFloor() {
        return destinationFloor;
    }

    public void setDestinationFloror(int destinationFloor) {
	this.destinationFloor = destinationFloor;
    }

    public int getCurrentFloor() {
	return currentFloor;
    }

    public void setCurrentFloor(int currentFloor) {
	this.currentFloor = currentFloor;
    }
	
    public boolean arrived() {
	return currentFloor == destinationFloor;
    }
	
    public boolean lower() {
	return currentFloor < destinationFloor;
    }
	
    public boolean higher() {
	return currentFloor > destinationFloor;
    }
        
    public void pressButton() {       
    }
}
