/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dataox.elevator;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author arkhi
 */
public class Lift {
    
    private static final int MAX_NUMBER_OF_PASSSENGERS = 5;
    private int currentFloor;
    private final List<Passenger> passengers = new LinkedList<>();
    private List<Floor> floors = new ArrayList<>();
    private static final Logger LOGGER = Logger.getLogger(Elevator.class.getName());
	
    public Lift(List<Floor> floors) {
	this.floors = floors;
    }
	
    public void addPassenger() {
	if (passengers.size() >= MAX_NUMBER_OF_PASSSENGERS) {
            return;
	}
        Floor floor = floors.get(currentFloor);
        Passenger pass = floor.removeWaitingPassenger();
        passengers.add(pass);
    }
	
    public void removePassenger(Passenger pass) {
        passengers.remove(pass);
        Floor floor = floors.get(currentFloor);
        floor.addWaitingPassenger(pass);
    }
	
    public void moveUp() {
        Optional<Passenger> destinationFloors = passengers.stream().
        filter(p->p.lower()).findAny();
        
        while(destinationFloors.isPresent()) {
            currentFloor++;
            for (Passenger pass : passengers ) {
                if (pass.getDestinationFloor() == currentFloor) {
                    removePassenger(pass);
                }
            }
            Floor floor = floors.get(currentFloor);
            while(passengers.size() < MAX_NUMBER_OF_PASSSENGERS && floor.hasPassengers()) {
                addPassenger();
            }
        LOGGER.log(Level.INFO, "Current floor{0}", currentFloor);
        LOGGER.log(Level.INFO, "Passengers in elevatror: {0} | destination floors: {1}", new Object[]{passengers.size(), floor.getPassengers().stream().
            mapToInt(p->p.getDestinationFloor()).toArray()});
        }
    }
	
    public void moveDown() {
		
    Optional<Passenger> destinationFloors = passengers.stream().
				filter(p->p.lower()).findAny();
	
    while(destinationFloors.isPresent()) {
        currentFloor++;
        Floor floor = floors.get(currentFloor);
        for (Passenger pass : passengers ) {
            if (pass.getDestinationFloor() == currentFloor) {
                removePassenger(pass);
            }
        }
            
        while(passengers.size() < MAX_NUMBER_OF_PASSSENGERS && floor.hasPassengers()) {
		addPassenger();
        }
			
        LOGGER.info(() -> "Current floor " + currentFloor);
        LOGGER.info(() -> "Passengers in elevatror: "+ passengers.size());
        }
    }
}
