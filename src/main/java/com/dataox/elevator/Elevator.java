/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.dataox.elevator;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


/**
 *
 * @author arkhi
 */
public class Elevator {

    static int getRandomNumber(int min, int max) {

	if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
	}

	Random r = new Random();
	return r.nextInt((max - min) + 1) + min;
    }
    
    public static void main(String[] args) {
        int numberofOfFloors = getRandomNumber(5, 20);
        List<Floor> floors = new ArrayList<>();
	for(int i = 0; i < numberofOfFloors; ++i) {            
            Floor floor = new Floor(i);
            floors.add(floor);
	}
		
        Lift lift = new Lift(floors);
	lift.moveUp();
	lift.moveDown();
	System.out.println("Finished succesfuly!");
    }
}
